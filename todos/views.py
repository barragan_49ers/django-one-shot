from django.shortcuts import render
from todos.models import TodoList


def todo_list_list(request):
    #gets all the instances of the TodoList and
    #todolists = model.object.getall()
    todolists = TodoList.objects.all()
    context ={
    #puts all instances of the the Todolist model in preperation for HTML call
        "todolists": todolists,
              }
    return render(request, "list.html", context)



# #details-->indicates--> instance
# def todo_list_detail(request):

#     #show details-->attributes
#     #create a model instance that is dynamic and allows for editing/view
#     #shows a 'particular'-->indicates-->accessible-->  id
#     list_details = TodoList.objects.get(id=id)

#     #make List_details accessible
#     #context = { name_of_view_(): list_details, }
#     context = { "todo_list_detail": list_details, }

# #input the template that will be accessing this specific view function/context
# #return render(request, list.html, context

# return render(request, "todos/list.html", context)
