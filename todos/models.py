from django.db import models

#class1
class TodoList(models.Model):

    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

# #class2
# class TodoItem (models.Model):
#     task = models.TextField(max_length=100)
#     due_date = models.DateTimeField(null=True, blank=True)
#     is_completed = models.BooleanField(default=False)
#     #create relationship i.e. the task belongs to list1, list3, or L4
#     list = models.ForeignKey(
#           TodoList, on_delete=models.CASCADE
#         )
