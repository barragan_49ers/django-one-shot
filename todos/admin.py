from django.contrib import admin
from todos.models import TodoList #,TodoItem


@admin.register(TodoList)
class TodosAdmin(admin.ModelAdmin):

#the list that will be delivered on the webpage, not before being formated in a table in html
    list_display  = [

        #contains the attributes of model.py class#1
        #attributes called on HTML, so that they can be formatted --feature7
        "name",
    ]


# @admin.register(TodoItem)
# class TodosAdmin(admin.ModelAdmin):

#     list_display  = [
#         #"task",
#         #"due_date",
#         #display whether or not the todo item is completed
#         #"is_completed"
#         "list"

#     ]
