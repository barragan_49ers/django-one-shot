from django.urls import path
from todos.views import todo_list_list



#Register that view in the todos app for the path "" and the name "todo_list_list" in
 #a new file named todos/urls.py.


urlpatterns =[
    path("", todo_list_list, name="todo_list_list")#helps project locate my application view function

    # #register view functions, for for viewing details--->details indicates instance
    # path("<int:id>/","todo_list_detail")
]
